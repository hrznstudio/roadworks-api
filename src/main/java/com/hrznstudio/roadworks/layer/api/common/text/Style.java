/*
 * This file is part of Roadworks Layer API, licensed under the MIT License (MIT).
 *
 * Copyright (c) Horizon Studio <contact@hrznstudio.com> <https://hrznstudio.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.hrznstudio.roadworks.layer.api.common.text;

import javax.annotation.Nullable;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

public class Style {
    public static final Style NONE = new Style(null, null, null, null, null);
    public static final Style BOLD = new Style(true, null, null, null, null);
    public static final Style ITALIC = new Style(null, true, null, null, null);
    public static final Style UNDERLINE = new Style(null, null, true, null, null);
    public static final Style STRIKETHROUGH = new Style(null, null, null, true, null);
    public static final Style OBFUSCATED = new Style(null, null, null, null, true);

    protected final Boolean bold, italic, underline, strikethrough, obfuscated;

    private Style(@Nullable Boolean bold, @Nullable Boolean italic, @Nullable Boolean underline, @Nullable Boolean strikethrough, @Nullable Boolean obfuscated) {
        this.bold = bold;
        this.italic = italic;
        this.underline = underline;
        this.strikethrough = strikethrough;
        this.obfuscated = obfuscated;
    }

    @Nullable
    private static Boolean compose(@Nullable Boolean bool1, @Nullable Boolean bool2) {
        if (bool1 == null) {
            return bool2;
        } else if (bool2 == null) {
            return bool1;
        } else if (bool1 == bool2) {
            return null;
        } else {
            return bool1;
        }
    }

    @Nullable
    private static Boolean negate(@Nullable Boolean bool) {
        if (bool != null)
            return !bool;
        return null;
    }

    public boolean isEmpty() {
        return !(bold != null || italic != null || underline != null || strikethrough != null || obfuscated != null);
    }

    public Style bold(@Nullable Boolean bold) {
        return new Style(
                bold,
                this.italic,
                this.underline,
                this.strikethrough,
                this.obfuscated
        );
    }

    public Style italic(@Nullable Boolean italic) {
        return new Style(
                this.bold,
                italic,
                this.underline,
                this.strikethrough,
                this.obfuscated
        );
    }

    public Style underline(@Nullable Boolean underline) {
        return new Style(
                this.bold,
                this.italic,
                underline,
                this.strikethrough,
                this.obfuscated
        );
    }

    public Style strikethrough(@Nullable Boolean strikethrough) {
        return new Style(
                this.bold,
                this.italic,
                this.underline,
                strikethrough,
                this.obfuscated
        );
    }

    public Style obfuscated(@Nullable Boolean obfuscated) {
        return new Style(
                this.bold,
                this.italic,
                this.underline,
                this.strikethrough,
                obfuscated
        );
    }

    public Optional<Boolean> isBold() {
        return Optional.ofNullable(this.bold);
    }

    public Optional<Boolean> isItalic() {
        return Optional.ofNullable(this.italic);
    }

    public Optional<Boolean> hasUnderline() {
        return Optional.ofNullable(this.underline);
    }

    public Optional<Boolean> hasStrikethrough() {
        return Optional.ofNullable(this.strikethrough);
    }

    public Optional<Boolean> isObfuscated() {
        return Optional.ofNullable(this.obfuscated);
    }

    public Style and(Style... styles) {
        checkNotNull(styles, "styles");
        if (styles.length == 0) {
            return this;
        } else if (this.isEmpty() && styles.length == 1) {
            return checkNotNull(styles[0], "style");
        }

        Boolean newBold = this.bold;
        Boolean newItalic = this.italic;
        Boolean newUnderline = this.underline;
        Boolean newStrikethrough = this.strikethrough;
        Boolean newObfuscated = this.obfuscated;

        for (Style style : styles) {
            checkNotNull(style, "style");
            newBold = compose(newBold, style.bold);
            newItalic = compose(newItalic, style.italic);
            newUnderline = compose(newUnderline, style.underline);
            newStrikethrough = compose(newStrikethrough, style.strikethrough);
            newObfuscated = compose(newObfuscated, style.obfuscated);
        }
        return new Style(newBold, newItalic, newUnderline, newStrikethrough, newObfuscated);
    }

    public Style andNot(Style... styles) {
        checkNotNull(styles, "styles");
        if (styles.length == 0) {
            return this;
        } else if (this.isEmpty() && styles.length == 1) {
            return checkNotNull(styles[0], "style");
        }

        Boolean newBold = this.bold;
        Boolean newItalic = this.italic;
        Boolean newUnderline = this.underline;
        Boolean newStrikethrough = this.strikethrough;
        Boolean newObfuscated = this.obfuscated;

        for (Style style : styles) {
            checkNotNull(style, "style");
            newBold = compose(newBold, negate(style.bold));
            newItalic = compose(newItalic, negate(style.italic));
            newUnderline = compose(newUnderline, negate(style.underline));
            newStrikethrough = compose(newStrikethrough, negate(style.strikethrough));
            newObfuscated = compose(newObfuscated, negate(style.obfuscated));
        }
        return new Style(newBold, newItalic, newUnderline, newStrikethrough, newObfuscated);
    }
}