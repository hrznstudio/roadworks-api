package com.hrznstudio.roadworks.layer.api.common.util;

import javax.annotation.Nonnull;

public class Identifier {
    public static final String DEFAULT_NAMESPACE = "minecraft";

    private final String namespace;
    private final String path;

    public Identifier(String namespace, String path) {
        this.namespace = namespace;
        this.path = path;
    }

    public static Identifier fromString(String path) {
        int index = path.indexOf(':');
        if (index != -1) {
            return new Identifier(path.substring(0, index), path.substring(index + 1));
        }
        return new Identifier(DEFAULT_NAMESPACE, path);
    }

    @Nonnull
    public String getNamespace() {
        return namespace;
    }

    @Nonnull
    public String getPath() {
        return path;
    }

    @Override
    public int hashCode() {
        return namespace.hashCode() + path.hashCode() * 31;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return  true;

        if (obj instanceof Identifier) {
            Identifier identifier = (Identifier) obj;
            return identifier.namespace.equals(namespace) && identifier.path.equals(path);
        }

        return false;
    }

    @Override
    public String toString() {
        return namespace + ":" + path;
    }
}
