package com.hrznstudio.roadworks.layer.api.common.block;

import com.hrznstudio.roadworks.layer.api.common.math.Position;
import com.hrznstudio.roadworks.layer.api.common.state.BlockState;
import com.hrznstudio.roadworks.layer.api.common.state.FluidState;

public interface BlockReader {
    BlockState getBlockState(Position pos);

    FluidState getFluidState(Position pos);
}
