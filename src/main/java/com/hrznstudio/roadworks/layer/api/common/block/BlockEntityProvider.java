package com.hrznstudio.roadworks.layer.api.common.block;

import com.hrznstudio.roadworks.layer.api.common.block.entity.BlockEntity;
import com.hrznstudio.roadworks.layer.api.common.state.BlockState;

public interface BlockEntityProvider<E extends BlockEntity> {
    boolean hasBlockEntity(BlockState state);

    boolean isBlockEntityTickable(BlockState state);

    E createBlockEntity(BlockState state);
}
