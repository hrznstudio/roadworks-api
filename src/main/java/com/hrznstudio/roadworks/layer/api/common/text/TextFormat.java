/*
 * This file is part of Roadworks Layer API, licensed under the MIT License (MIT).
 *
 * Copyright (c) Horizon Studio <contact@hrznstudio.com> <https://hrznstudio.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.hrznstudio.roadworks.layer.api.common.text;

import static com.google.common.base.Preconditions.checkNotNull;

public class TextFormat {
    public static final TextFormat NONE = new TextFormat(TextColour.NONE, Style.NONE);
    private final TextColour colour;
    private final Style style;

    private TextFormat(TextColour colour, Style style) {
        this.colour = checkNotNull(colour, "colour");
        this.style = checkNotNull(style, "style");
    }

    public static TextFormat of() {
        return NONE;
    }

    public static TextFormat of(TextColour colour, Style style) {
        return new TextFormat(colour, style);
    }

    public static TextFormat of(TextColour colour) {
        return new TextFormat(colour, Style.NONE);
    }

    public static TextFormat of(Style style) {
        return new TextFormat(TextColour.NONE, style);
    }

    public TextFormat style(Style style) {
        return new TextFormat(this.colour, style);
    }

    public TextFormat colour(TextColour colour) {
        return new TextFormat(colour, this.style);
    }

    public TextColour getcolour() {
        return colour;
    }

    public Style getStyle() {
        return style;
    }
}