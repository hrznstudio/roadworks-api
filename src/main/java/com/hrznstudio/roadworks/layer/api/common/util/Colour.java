/*
 * This file is part of Roadworks Layer API, licensed under the MIT License (MIT).
 *
 * Copyright (c) Horizon Studio <contact@hrznstudio.com> <https://hrznstudio.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.hrznstudio.roadworks.layer.api.common.util;

import com.google.common.base.MoreObjects;

import java.util.Objects;

public class Colour {
    private static final int MASK = 0xFF;

    public static final Colour BLACK = ofRgb(0x000000);

    public static final Colour GRAY = ofRgb(0x808080);

    public static final Colour WHITE = ofRgb(0xFFFFFF);

    public static final Colour BLUE = ofRgb(0x0000FF);

    public static final Colour GREEN = ofRgb(0x008000);

    public static final Colour LIME = ofRgb(0x00FF00);

    public static final Colour RED = ofRgb(0xFF0000);

    public static final Colour YELLOW = ofRgb(0xFFFF00);

    public static final Colour MAGENTA = ofRgb(0xFF00FF);

    public static final Colour PURPLE = ofRgb(0xAA00FF);

    public static final Colour DARK_CYAN = ofRgb(0x008B8B);

    public static final Colour DARK_GREEN = ofRgb(0x006400);

    public static final Colour DARK_MAGENTA = ofRgb(0x8B008B);

    public static final Colour CYAN = ofRgb(0x00FFFF);

    public static final Colour NAVY = ofRgb(0x000080);

    public static final Colour PINK = ofRgb(0xFF00AA);
    private final byte red;
    private final byte green;
    private final byte blue;
    private final int rgb;

    private Colour(int red, int green, int blue) {
        this.red = (byte) (red & MASK);
        this.green = (byte) (green & MASK);
        this.blue = (byte) (blue & MASK);
        this.rgb = ((this.red & MASK) << 0x10)
                | ((this.green & MASK) << 0x8)
                | ((this.blue & MASK));
    }

    public static Colour ofRgb(int hex) {
        return ofRgb((hex >> 0x10) & MASK, (hex >> 0x8) & MASK, hex & MASK);
    }

    public static Colour ofRgb(int red, int green, int blue) {
        return new Colour(red, green, blue);
    }

    public int getRed() {
        return MASK & this.red;
    }

    public int getGreen() {
        return MASK & this.green;
    }

    public int getBlue() {
        return MASK & this.blue;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("red", this.getRed())
                .add("green", this.getGreen())
                .add("blue", this.getBlue())
                .toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(Colour.class, this.red, this.green, this.blue);
    }

    public int getRgb() {
        return this.rgb;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Colour other = (Colour) obj;
        return this.red == other.red && this.green == other.green && this.blue == other.blue;
    }
}