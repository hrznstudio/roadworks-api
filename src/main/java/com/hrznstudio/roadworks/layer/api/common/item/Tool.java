package com.hrznstudio.roadworks.layer.api.common.item;

import com.google.common.collect.Maps;
import com.hrznstudio.roadworks.layer.api.common.state.BlockState;

import java.util.Collection;
import java.util.Map;
import java.util.regex.Pattern;

public interface Tool {

    boolean canHarvest(ItemStack stack, BlockState state);

    float getSpeed(ItemStack stack, BlockState state);

    ToolTier getToolTier();

    Collection<ToolCategory> getToolCategories(ItemStack stack);

    interface ToolTier {
        int getMaxUses();

        float getEfficiency();

        float getAttackDamage();

        int getHarvestLevel();

        int getEnchantability();
    }

    class ToolCategory {
        private static final Pattern VALID_NAME = Pattern.compile("[^a-z_]"); //Only a-z and _ are allowed, meaning names must be lower case. And use _ to separate words.
        private static final Map<String, ToolCategory> values = Maps.newHashMap();

        public static final ToolCategory AXE = get("axe");
        public static final ToolCategory PICKAXE = get("pickaxe");
        public static final ToolCategory SHOVEL = get("shovel");

        public static ToolCategory get(String name) {
            if (VALID_NAME.matcher(name).find())
                throw new IllegalArgumentException("ToolCategory called with invalid name: " + name);
            return values.putIfAbsent(name, new ToolCategory(name));
        }

        private final String name;

        private ToolCategory(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
