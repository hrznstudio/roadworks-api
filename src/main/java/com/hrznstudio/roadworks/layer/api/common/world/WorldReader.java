package com.hrznstudio.roadworks.layer.api.common.world;

import com.hrznstudio.roadworks.layer.api.common.block.BlockReader;

public interface WorldReader extends BlockReader {
}
