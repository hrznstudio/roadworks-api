/*
 * This file is part of Roadworks Layer API, licensed under the MIT License (MIT).
 *
 * Copyright (c) Horizon Studio <contact@hrznstudio.com> <https://hrznstudio.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.hrznstudio.roadworks.layer.api.common.text;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.hrznstudio.roadworks.layer.api.common.text.action.ClickAction;
import com.hrznstudio.roadworks.layer.api.common.text.action.HoverAction;
import com.hrznstudio.roadworks.layer.api.common.text.action.ShiftClickAction;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

public abstract class Text implements Comparable<Text> {
    public static final Text EMPTY = of(null);
    private static final char NEW_LINE_CHAR = '\n';
    private static final String NEW_LINE_STRING = "\n";
    public static final Text NEW_LINE = of(NEW_LINE_STRING);

    private final TextFormat format;
    private final ImmutableList<Text> children;
    private final ClickAction<?> clickAction;
    private final HoverAction<?> hoverAction;
    private final ShiftClickAction<?> shiftClickAction;

    Text() {
        this.format = TextFormat.NONE;
        this.children = ImmutableList.of();
        this.clickAction = null;
        this.hoverAction = null;
        this.shiftClickAction = null;
    }

    Text(TextFormat format, ImmutableList<Text> children, @Nullable ClickAction<?> clickAction, @Nullable HoverAction<?> hoverAction, @Nullable ShiftClickAction<?> shiftClickAction) {
        this.format = checkNotNull(format, "format");
        this.children = checkNotNull(children, "children");
        this.clickAction = clickAction;
        this.hoverAction = hoverAction;
        this.shiftClickAction = shiftClickAction;
    }

    public static Text of(String s) {
        if (Strings.isNullOrEmpty(s))
            return StringText.EMPTY;
        return new StringText(s);
    }

    public Optional<ClickAction<?>> getClickAction() {
        return Optional.ofNullable(clickAction);
    }

    public Optional<HoverAction<?>> getHoverAction() {
        return Optional.ofNullable(hoverAction);
    }

    public Optional<ShiftClickAction<?>> getShiftClickAction() {
        return Optional.ofNullable(shiftClickAction);
    }

    public abstract static class Builder {
        protected TextFormat format = TextFormat.NONE;
        protected List<Text> children = new ArrayList<>();
        protected ClickAction<?> clickAction;
        protected HoverAction<?> hoverAction;
        protected ShiftClickAction<?> shiftClickAction;

        protected Builder() {
        }
    }
}