package com.hrznstudio.roadworks.layer.api.common.block;

import com.hrznstudio.roadworks.layer.api.common.fluid.Fluid;
import com.hrznstudio.roadworks.layer.api.common.math.Position;
import com.hrznstudio.roadworks.layer.api.common.state.BlockState;
import com.hrznstudio.roadworks.layer.api.common.state.FluidState;
import com.hrznstudio.roadworks.layer.api.common.world.World;

import java.util.Optional;

public interface IFluidHolder {

    default boolean canContainFluid(BlockReader world, Position position, BlockState state, Fluid fluid) {
        return false;
    }

    default FluidState getFluidState(World world, Position position, BlockState state) {
        return null; //TODO: make empty
    }

    default boolean setFluidState(World world, Position position, BlockState state, FluidState fluidState) {
        return false;
    }

    default Optional<Fluid> emptyFluidState(World world, Position position, BlockState state) {
        return Optional.empty();
    }
}