package com.hrznstudio.roadworks.layer.api.common.item;

import com.hrznstudio.roadworks.layer.api.common.text.TextColour;

public enum Rarity {
    COMMON(TextColour.WHITE),
    UNCOMMON(TextColour.YELLOW),
    RARE(TextColour.AQUA),
    EPIC(TextColour.LIGHT_PURPLE);

    private final TextColour colour;

    Rarity(TextColour colour) {
        this.colour = colour;
    }

    public TextColour getColour() {
        return colour;
    }
}
