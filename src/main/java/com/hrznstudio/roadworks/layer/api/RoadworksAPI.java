package com.hrznstudio.roadworks.layer.api;

import com.hrznstudio.roadworks.loader.api.Environment;

public interface RoadworksAPI {
    Environment getEnvironment();

    boolean isEnabled(String plugin);
}
