/*
 * This file is part of Roadworks Layer API, licensed under the MIT License (MIT).
 *
 * Copyright (c) Horizon Studio <contact@hrznstudio.com> <https://hrznstudio.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.hrznstudio.roadworks.layer.api.common.math;

import com.hrznstudio.roadworks.layer.api.common.world.Facing;

public interface Position extends Vec3i {

    Factory FACTORY = (x, y, z) -> {
        throw new UnsupportedOperationException("Position factory not overridden");
    };

    static Position create(int x, int y, int z) {
        return FACTORY.create(x, y, z);
    }

    Position offset(Facing facing, int amount);

    Position offset(Facing facing);

    MutablePosition toMutable();

    Position toImmutable();

    interface Factory {
        Position create(int x, int y, int z);
    }
}
