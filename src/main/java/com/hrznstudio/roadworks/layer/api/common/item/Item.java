/*
 * This file is part of Roadworks Layer API, licensed under the MIT License (MIT).
 *
 * Copyright (c) Horizon Studio <contact@hrznstudio.com> <https://hrznstudio.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.hrznstudio.roadworks.layer.api.common.item;

import com.hrznstudio.roadworks.layer.api.common.player.Player;
import com.hrznstudio.roadworks.layer.api.common.script.GameObject;
import com.hrznstudio.roadworks.layer.api.common.text.Text;
import com.hrznstudio.roadworks.layer.api.common.world.World;

import javax.annotation.Nonnull;
import java.util.List;

public interface Item extends GameObject {

    void addDetails(@Nonnull Key key, @Nonnull ItemStack stack, @Nonnull World world, @Nonnull Player player, @Nonnull List<Text> tooltip);

    String getTranslationKey(ItemStack stack);

    enum Key {
        NONE,
        ALT,
        CTRL,
        SHIFT
    }
}