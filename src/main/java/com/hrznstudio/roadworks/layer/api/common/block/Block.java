/*
 * This file is part of Roadworks Layer API, licensed under the MIT License (MIT).
 *
 * Copyright (c) Horizon Studio <contact@hrznstudio.com> <https://hrznstudio.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.hrznstudio.roadworks.layer.api.common.block;

import com.hrznstudio.roadworks.layer.api.common.Activation;
import com.hrznstudio.roadworks.layer.api.common.math.Position;
import com.hrznstudio.roadworks.layer.api.common.math.Vec3i;
import com.hrznstudio.roadworks.layer.api.common.player.Hand;
import com.hrznstudio.roadworks.layer.api.common.player.Player;
import com.hrznstudio.roadworks.layer.api.common.script.GameObject;
import com.hrznstudio.roadworks.layer.api.common.state.BlockState;
import com.hrznstudio.roadworks.layer.api.common.world.Facing;
import com.hrznstudio.roadworks.layer.api.common.world.World;

public interface Block extends GameObject {
    default boolean isCube() {
        return true;
    }

    default RenderType getRenderType(BlockState state) {
        return RenderType.MODEL;
    }

    default RenderLayer getRenderLayer() {
        return RenderLayer.SOLID;
    }

    default boolean canRenderInLayer(RenderLayer layer, BlockState state) {
        return layer == getRenderLayer();
    }

    default Activation onBlockUsed(World world, Player player, Position pos, BlockState state, Hand hand, Facing side, Vec3i hit) {
        return Activation.IGNORE;
    }

    default Activation onBlockClicked(World world, Player player, Position pos, BlockState state) {
        return Activation.IGNORE;
    }

    enum RenderType {
        /**
         * Skip all rendering
         */
        INVISIBLE,
        /**
         * Special rendering only
         */
        ANIMATED,
        /**
         * Model and special rendering
         */
        MODEL
    }

    enum RenderLayer {
        SOLID,
        CUTOUT_MIPPED,
        CUTOUT,
        TRANSLUCENT
    }
}