package com.hrznstudio.roadworks.layer.api.common.world;

import com.hrznstudio.roadworks.layer.api.common.math.Position;
import com.hrznstudio.roadworks.layer.api.common.state.BlockState;

public interface WorldWriter {
    void setBlockState(Position pos, BlockState state);

    void setBlockState(Position pos, BlockState state, Flags... flags);

    enum Flags {
        UPDATE(1),
        CLIENT_SYNC(2),
        PREVENT_RERENDER(4),
        FORCE_MAIN_RENDER(8),
        PREVENT_NEIGHBOR(16),
        PREVENT_NEIGHBOR_DROP(32),
        MOVING(64);

        private int id;

        Flags(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }
    }
}