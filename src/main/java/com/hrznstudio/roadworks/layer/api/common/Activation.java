package com.hrznstudio.roadworks.layer.api.common;

public enum Activation {
    SUCCESS,
    IGNORE,
    FAILURE
}
