/*
 * This file is part of Roadworks Layer API, licensed under the MIT License (MIT).
 *
 * Copyright (c) Horizon Studio <contact@hrznstudio.com> <https://hrznstudio.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.hrznstudio.roadworks.layer.api.common.text;

import com.hrznstudio.roadworks.layer.api.common.util.Colour;

import java.util.Optional;

public class TextColour {
    public static final TextColour BLACK = new TextColour("BLACK", '0', 0);
    public static final TextColour DARK_BLUE = new TextColour("DARK_BLUE", '1', 170);
    public static final TextColour DARK_GREEN = new TextColour("DARK_GREEN", '2', 43520);
    public static final TextColour DARK_AQUA = new TextColour("DARK_AQUA", '3', 43690);
    public static final TextColour DARK_RED = new TextColour("DARK_RED", '4', 11141120);
    public static final TextColour DARK_PURPLE = new TextColour("DARK_PURPLE", '5', 11141290);
    public static final TextColour GOLD = new TextColour("GOLD", '6', 16755200);
    public static final TextColour GRAY = new TextColour("GRAY", '7', 11184810);
    public static final TextColour DARK_GRAY = new TextColour("DARK_GRAY", '8', 5592405);
    public static final TextColour BLUE = new TextColour("BLUE", '9', 5592575);
    public static final TextColour GREEN = new TextColour("GREEN", 'a', 5635925);
    public static final TextColour AQUA = new TextColour("AQUA", 'b', 5636095);
    public static final TextColour RED = new TextColour("RED", 'c', 16733525);
    public static final TextColour LIGHT_PURPLE = new TextColour("LIGHT_PURPLE", 'd', 16733695);
    public static final TextColour YELLOW = new TextColour("YELLOW", 'e', 16777045);
    public static final TextColour WHITE = new TextColour("WHITE", 'f', 16777215);

    public static TextColour NONE = new TextColour("NONE", ' ', null);
    private final String name;
    private final char code;
    private final Colour colour;

    public TextColour(String name, char code, Colour colour) {
        this.name = name;
        this.code = code;
        this.colour = colour;
    }

    public TextColour(String name, char code, int colour) {
        this(name, code, Colour.ofRgb(colour));
    }

    public Optional<Colour> getColour() {
        return Optional.ofNullable(colour);
    }
}