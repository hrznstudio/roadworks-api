package com.hrznstudio.roadworks.layer.api.common.item;

import com.hrznstudio.roadworks.layer.api.common.player.Player;
import com.hrznstudio.roadworks.layer.api.common.world.World;

import java.util.Collection;

public interface Food {
    int getFoodAmount();

    float getSaturationAmount();

    Collection<FoodCategory> getCategories();

    void onEaten(ItemStack stack, World world, Player player);

    enum FoodCategory {
        MEAT,
        VEGETABLE,
        FRUIT,
        OTHER
    }
}