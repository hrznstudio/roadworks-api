package com.hrznstudio.roadworks.layer.api.common.permission;

public interface PermissionClient {
    boolean hasPermission(Node node);
}
