package com.hrznstudio.roadworks.layer.api.common.block.entity;

import com.hrznstudio.roadworks.layer.api.common.block.Block;
import com.hrznstudio.roadworks.layer.api.common.block.BlockEntityProvider;
import com.hrznstudio.roadworks.layer.api.common.math.Position;
import com.hrznstudio.roadworks.layer.api.common.script.GameObject;
import com.hrznstudio.roadworks.layer.api.common.state.BlockState;
import com.hrznstudio.roadworks.layer.api.common.world.World;

public interface BlockEntity extends GameObject {
    <B extends Block & BlockEntityProvider> B getBlock();

    Position getPos();

    World getWorld();

    BlockState getState();

    /**
     * Runs on game tick.
     * Requires {@link BlockEntityProvider#isBlockEntityTickable(BlockState)} to return true.
     */
    void onTick();
}