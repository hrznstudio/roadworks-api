package com.hrznstudio.roadworks.layer.api.common.item;

import com.hrznstudio.roadworks.layer.api.common.text.Text;

import javax.annotation.Nullable;

public interface ItemStack {
    ItemStack EMPTY = of(null);

    Factory FACTORY = (item, count) -> {
        throw new UnsupportedOperationException("Vec3 creation factory not overridden");
    };

    interface Factory {
        ItemStack create(@Nullable Item item, int count);
    }

    static ItemStack of(@Nullable Item item){
        return FACTORY.create(item, 1);
    }

    static ItemStack of(@Nullable Item item, int count){
        return FACTORY.create(item, count);
    }

    Item getItem();

    int getCount();

    ItemStack setCount(int i);

    boolean isEmpty();

    boolean isStackable();

    ItemStack shrink(int i);

    default ItemStack shrink() {
        return shrink(1);
    }

    ItemStack grow(int i);

    default ItemStack grow() {
        return grow(1);
    }

    boolean hasGlowEffect();

    Rarity getRarity();

    Text getCustomName();

    ItemStack setCustomName(Text text);

    ItemStack clearCustomName();

    boolean hasCustomName();
}