package com.hrznstudio.roadworks.layer.api.server;

public enum GameType {
    NOT_SET(-1),
    SURVIVAL(0),
    CREATIVE(1),
    ADVENTURE(2),
    SPECTATOR(3);

    public static final GameType[] VALUES = values();

    private int id;

    GameType(int id) {
        this.id = id;
    }

    public static GameType getByID(int id) {
        return getByIDWithDefault(id, SURVIVAL);
    }

    public static GameType getByIDWithDefault(int id, GameType defaultType) {
        for (GameType type : VALUES) {
            if (type.id == id) {
                return type;
            }
        }

        return defaultType;
    }

    public int getId() {
        return id;
    }
}