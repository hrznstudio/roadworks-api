package com.hrznstudio.roadworks.layer.api.common.math;

import org.joml.Vector3i;
import org.joml.Vector3ic;

public interface Vec3i {
    Factory VEC3I_FUNCTION = (x, y, z) -> {
        throw new UnsupportedOperationException("Vec3i creation factory not overridden");
    };

    static Vec3i create(int x, int y, int z) {
        return VEC3I_FUNCTION.create(x, y, z);
    }

    static Vec3i fromJoml(Vector3ic vec) {
        return VEC3I_FUNCTION.create(vec.x(), vec.y(), vec.z());
    }

    int getX();

    int getY();

    int getZ();

    Vector3i toJoml();

    interface Factory {
        Vec3i create(int x, int y, int z);
    }
}