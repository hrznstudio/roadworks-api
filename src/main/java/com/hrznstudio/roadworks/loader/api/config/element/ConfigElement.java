package com.hrznstudio.roadworks.loader.api.config.element;

public interface ConfigElement {
    ConfigElementType getType();
}
