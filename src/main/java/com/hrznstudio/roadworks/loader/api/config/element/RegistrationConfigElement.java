package com.hrznstudio.roadworks.loader.api.config.element;

import com.google.common.collect.ImmutableMap;
import com.hrznstudio.roadworks.layer.api.common.script.GameObject;
import com.hrznstudio.roadworks.layer.api.common.util.Identifier;

public final class RegistrationConfigElement<T extends GameObject> implements ConfigElement {
    public static final ConfigElementType TYPE = ConfigElementType.create("registrations");

    private final Class<T> registryType;
    private final ImmutableMap<Identifier, T> entries;

    private RegistrationConfigElement(Class<T> registryType, ImmutableMap<Identifier, T> entries) {
        this.registryType = registryType;
        this.entries = entries;
    }

    public Class<T> getRegistryType() {
        return registryType;
    }

    @Override
    public ConfigElementType getType() {
        return TYPE;
    }

    public static <T extends GameObject> Builder<T> builder(Class<T> registryType) {
        return new Builder<>(registryType);
    }

    public static class Builder<T extends GameObject> {
        private final Class<T> registryType;
        private final ImmutableMap.Builder<Identifier, T> entries = ImmutableMap.builder();

        private Builder(Class<T> registryType) {
            this.registryType = registryType;
        }

        public Builder withEntry(Identifier identifier, T entry) {
            entries.put(identifier, entry);
            return this;
        }

        public RegistrationConfigElement<T> build() {
            return new RegistrationConfigElement<>(registryType, entries.build());
        }
    }
}
