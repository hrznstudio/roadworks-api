package com.hrznstudio.roadworks.loader.api.config;

import com.github.zafarkhaja.semver.Version;
import com.google.common.collect.ImmutableMultimap;
import com.hrznstudio.roadworks.loader.api.config.element.ConfigElement;
import com.hrznstudio.roadworks.loader.api.config.element.ConfigElementType;

public final class PluginConfigBuilder {
    private String modid, name;
    private Version version;
    private final ImmutableMultimap.Builder<ConfigElementType, ConfigElement> elements = ImmutableMultimap.builder();

    private PluginConfigBuilder() {
    }

    public static PluginConfigBuilder create() {
        return new PluginConfigBuilder();
    }

    public void configureModInfo(String modid, String modname, Version version) {
        this.modid = modid;
        this.name = modname;
        this.version = version;
    }

    public void configureElement(ConfigElement element) {
        elements.put(element.getType(), element);
    }

    public void configureElements(ConfigElement... elements) {
        for (ConfigElement element : elements)
            configureElement(element);
    }

    public PluginConfig build() {
        return new PluginConfig(modid, name, version, elements.build());
    }
}
