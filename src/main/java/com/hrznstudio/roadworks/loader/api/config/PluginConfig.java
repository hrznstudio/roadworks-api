package com.hrznstudio.roadworks.loader.api.config;

import com.github.zafarkhaja.semver.Version;
import com.google.common.collect.ImmutableMultimap;
import com.hrznstudio.roadworks.loader.api.config.element.ConfigElement;
import com.hrznstudio.roadworks.loader.api.config.element.ConfigElementType;

import java.util.Collection;

public final class PluginConfig {
    private final String modid, name;
    private final Version version;
    private final ImmutableMultimap<ConfigElementType, ConfigElement> elements;

    public PluginConfig(String modid, String name, Version version, ImmutableMultimap<ConfigElementType, ConfigElement> elements) {
        this.modid = modid;
        this.name = name;
        this.version = version;
        this.elements = elements;
    }

    public Collection<ConfigElement> getElements(ConfigElementType type) {
        return elements.get(type);
    }

    public Collection<ConfigElement> getElements() {
        return elements.values();
    }
}
