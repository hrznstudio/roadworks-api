package com.hrznstudio.roadworks.loader.api.config;

import com.hrznstudio.roadworks.loader.api.Environment;

public interface PluginConfigurator {
    default boolean test(Environment environment) {
        return true;
    }

    void configure(PluginConfigBuilder config);
}
