package com.hrznstudio.roadworks.loader.api;

public enum ReleaseStage {
    /**
     * Fully released
     */
    RELEASE,
    /**
     * Feature-Complete but with bugs
     */
    BETA,
    /**
     * Unstable, crash-prone.
     */
    ALPHA,
    /**
     * Still early in development
     */
    PRE_ALPHA,
    /**
     * Very early in development, Used for development builds.
     */
    DEVELOPMENT;
}
