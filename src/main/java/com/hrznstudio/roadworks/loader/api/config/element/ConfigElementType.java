package com.hrznstudio.roadworks.loader.api.config.element;

public interface ConfigElementType {
    static ConfigElementType create(String key) {
        return new StringKeyed(key);
    }

    class StringKeyed implements ConfigElementType {
        private final String key;

        StringKeyed(String key) {
            this.key = key;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }

            return obj instanceof StringKeyed && ((StringKeyed) obj).key.equals(key);
        }

        @Override
        public int hashCode() {
            return key.hashCode();
        }
    }
}
