package com.hrznstudio.roadworks.loader.api;

public interface Environment {
    boolean isPhysicalClient();

    boolean isPhysicalServer();
}
