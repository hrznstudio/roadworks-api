package com.hrznstudio.roadworks.loader.api;

public enum PluginType {
    /**
     * A normal plugin
     */
    PLUGIN,
    /**
     * An extension to a plugin
     */
    ADDON,
    /**
     * An official extension to a plugin
     */
    DLC;
}
