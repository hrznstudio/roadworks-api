package com.hrznstudio.roadworks.sdk.vr;

import java.util.List;

public interface Headset {
    List<Tracked> getTrackedItems();
}
