package com.hrznstudio.roadworks.sdk.input;

import java.util.Collection;
import java.util.Optional;

public interface ControllerManager {
    Optional<Controller> getController(int id);

    Collection<Controller> getConnectedControllers();

    Optional<Controller> getActiveController();

    void setActiveController(Controller controller);
}