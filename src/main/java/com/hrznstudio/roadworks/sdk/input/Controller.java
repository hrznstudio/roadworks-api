package com.hrznstudio.roadworks.sdk.input;

public interface Controller {

    String getName();

    boolean isDown(Button button) throws ControllerDisconnectedException;

    boolean isPressed(Button button) throws ControllerDisconnectedException;

    boolean isVibrating() throws ControllerDisconnectedException;

    boolean vibrate(float left, float right) throws ControllerDisconnectedException;

    boolean vibrate(float equal) throws ControllerDisconnectedException;

    boolean stopVibration() throws ControllerDisconnectedException;

    float getAxis(Axis axis, Stick stick) throws ControllerDisconnectedException;

    boolean isConnected();

    void update();

    enum Button {
        X,
        A,
        B,
        Y,
        START,
        MENU,
        TRIGGER_LEFT,
        TRIGGER_RIGHT,
        BUMPER_LEFT,
        BUMPER_RIGHT,
        DPAD_DOWN,
        DPAD_UP,
        DPAD_LEFT,
        DPAD_RIGHT,
        STICK_LEFT,
        STICK_RIGHT,
        UNKNOWN;
    }

    enum Axis {
        X,
        Y
    }

    enum Stick {
        LEFT,
        RIGHT
    }
}