package com.hrznstudio.roadworks.sdk.input;

public class ControllerDisconnectedException extends RuntimeException {
    private Controller controller;

    public ControllerDisconnectedException(Controller controller) {
        super("Controller attempted to be accessed while disconnected");
        this.controller = controller;
    }

    public Controller getController() {
        return controller;
    }
}
